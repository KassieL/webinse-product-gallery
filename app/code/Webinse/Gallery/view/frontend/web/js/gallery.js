define([
    "jquery",
    "jquery/ui",
    "Webinse_Gallery/js/picturefill.min",
    "Webinse_Gallery/js/lightgallery.min",
    "Webinse_Gallery/js/lg-thumbnail.min",
    "Webinse_Gallery/js/lg-pager.min",
    "Webinse_Gallery/js/lg-autoplay.min",
    "Webinse_Gallery/js/lg-fullscreen.min",
    "Webinse_Gallery/js/lg-zoom.min",
    "Webinse_Gallery/js/lg-hash.min",
    "Webinse_Gallery/js/lg-share.min",
], function ($, str) {
    return function (config){
        $(document).ready(function(){
            if (config.animatedThumb === '1') {
                console.log('base enable');
                lightGallery(document.getElementById('aniimated-thumbnials'));
            } else{
                console.log('base disable');
                lightGallery(document.getElementById('aniimated-thumbnials'), {
                    thumbnail:true,
                    animateThumb: false,
                    showThumbByDefault: false
                });
            }
        });
    };
});