<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Block for working with image collection
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */

namespace Webinse\Gallery\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlInterface;
use Webinse\Gallery\Model\ImageFactory;
use Webinse\Gallery\Model\Image as ImageModel;
use Webinse\Gallery\Model\Config;

class Image extends Template
{
    /**
     * Image factory
     *
     * @var ImageFactory
     */
    protected $_imageFactory;
    /**
     * @var Config
     */
    protected $_config;

    /**
     * @param Context $context
     * @param ImageFactory $imageFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        ImageFactory $imageFactory,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_imageFactory = $imageFactory;
        $this->_config = $config;
    }


    public function getImages()
    {
        try{
            $directory_data = $this->getRequest()->getParams();
            $directory = "pub/media/albums/".$directory_data['album']. DIRECTORY_SEPARATOR .$directory_data['gallery'];
            $images = scandir($directory);
            return $images;
        }catch (\Exception $e) {
            $e->getMessage();
        }
        return [];
    }

    public function getDirectoryData(){
        $directory_data = $this->getRequest()->getParams();
        return $directory_data;
    }

    public function getConfigs(){
        return $this->_config->getAnimatedThmbnailsStatus();
    }
}