<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Block for working with images collection
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */

namespace Webinse\Gallery\Block\Image;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\UrlInterface;
use Webinse\Gallery\Model\ResourceModel\Image\CollectionFactory;
use Webinse\Gallery\Model\ResourceModel\Image\Collection;
use Webinse\Gallery\Api\Data\ImageInterface;

class All extends Template
{
    /**
     * Collection factory
     *
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * Retrieve all questions sorted by creation time
     *
     * @return Collection
     */
    public function getCollection()
    {
        $collection = $this->_collectionFactory->create();
        return $collection;
    }

    /**
     * @return string
     */
    public function getAddNewUrl()
    {
        return $this->getUrl("webinse_gallery/image/addNew");
    }

    public function getAlbums()
    {
        try{
            $directory = "pub/media/albums";
            $albums = scandir($directory);
            return $albums;
        }catch (\Exception $e) {
         $e->getMessage();
        }
        return [];
    }

    public function getGalleries($album)
    {
        try{
            $directory = "pub/media/albums/".$album;
            $galleries = scandir($directory);
            return $galleries;
        }catch (\Exception $e) {
            $e->getMessage();
        }
        return [];
    }

    public function getShowGalleryUrl($album, $gallery)
    {
        return $this->getUrl("webinse_gallery/image/showgallery/",['album' => $album, 'gallery' => $gallery]);
    }

}
