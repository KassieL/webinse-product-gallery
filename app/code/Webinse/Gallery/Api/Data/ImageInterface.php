<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Service Contract Data for Image model
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 MatesAcademy Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
namespace Webinse\Gallery\Api\Data;

interface ImageInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const IMAGE_ID   = 'image_id';
    const NAME       = 'name';
    const GALLERY    = 'gallery';
    const ALBUM      = 'album';

    /**

     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Get gallery
     *
     * @return string|null
     */
    public function getGallery();

    /**
     * Get album
     *
     * @return string|null
     */
    public function getAlbum();

    /**
     * Set ID
     *
     * @param int $id
     * @return ImageInterface
     */
    public function setId($id);

    /**
     * Set name
     *
     * @param string $name
     * @return ImageInterface
     */
    public function setName($name);

    /**
     * Set gallery
     *
     * @param string $gallery
     * @return ImageInterface
     */
    public function setGallery($gallery);

    /**
     * Set album
     *
     * @param string $album
     * @return ImageInterface
     */
    public function setAlbum($album);

}