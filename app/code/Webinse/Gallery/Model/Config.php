<?php
namespace Webinse\Gallery\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Config
{
    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    protected $storeId = null;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getExtensionStatus(){
        return $this->config('image_group/enable');
    }

    public function getAnimatedThmbnailsStatus(){
        return $this->config('image_group/enable_animation');
    }

    protected function config($code)
    {
        return $this->scopeConfig->getValue("image_config/{$code}", ScopeInterface::SCOPE_STORE, $this->storeId);
    }
}