<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Image model
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
namespace Webinse\Gallery\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Webinse\Gallery\Api\Data\ImageInterface;

class Image extends AbstractModel implements ImageInterface, IdentityInterface
{
    /**
     * Image cache tag
     */
    const CACHE_TAG = 'webinse_gallery_images';

    /**
     * @var string
     */
    protected $_cacheTag = 'webinse_gallery_images';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'webinse_gallery_images';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webinse\Gallery\Model\ResourceModel\Image');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Retrieve image id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::IMAGE_ID);
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Get gallery
     *
     * @return string|null
     */
    public function getGallery()
    {
        return $this->getData(self::GALLERY);
    }

    /**
     * Get album
     *
     * @return string|null
     */
    public function getAlbum()
    {
        return $this->getData(self::ALBUM);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return ImageInterface
     */
    public function setId($id)
    {
        return $this->setData(self::IMAGE_ID, $id);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ImageInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Set gallery
     *
     * @param string $gallery
     * @return ImageInterface
     */
    public function setGallery($gallery)
    {
        return $this->setData(self::GALLERY, $gallery);
    }

    /**
     * Set album
     *
     * @param string $album
     * @return ImageInterface
     */
    public function setAlbum($album)
    {
        return $this->setData(self::ALBUM, $album);
    }

}
