<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Grid
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Helper for the Gallery.
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
namespace Webinse\Gallery\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class DataValid
 * @package MateAcademy\FAQ4\Helper
 */
class DataValid extends AbstractHelper
{
    /**
     * one megabyte
     */
    const MB = 1048576;
    /**
     * @param $params
     * @param $reqParams
     * @return bool
     */
    public function validationOfParams($params, $reqParams)
    {
        //TODO: use array_diff
        foreach ($reqParams as $reqParam) {
            if (!isset($params[$reqParam])) {
                return false;
            }
        }
        foreach ($params as $key => $param) {
            if (in_array($key, $reqParams)) {
                if (!preg_match('/^[A-Za-z0-9.,+]/', $param)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function validationOfImage($image)
    {
        if ($image['size'] > 100 * self::MB) {
            return false;
        }
        return true;
    }
}