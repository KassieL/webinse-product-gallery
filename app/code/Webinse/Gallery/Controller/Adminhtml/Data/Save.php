<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Barcode
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Frontend Action backendName/data/save
 *
 * @category    Webinse
 * @package     Webinse_Barcode
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
namespace Webinse\Gallery\Controller\Adminhtml\Data;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Webinse\Gallery\Model\ImageFactory;
use Magento\Framework\File\UploaderFactory;
//use Webinse\Gallery\Model\Config;
//use Magento\Framework\Stdlib\DateTime\DateTime;
use Webinse\Gallery\Helper\DataValid;

/**
 * Class Save
 * @package MatesAcademy\FAQ4\Controller\Adminhtml\Data
 */
class Save extends Action
{
    const DIR_ALBUM = 'pub' . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'albums';
    /**
     * @var Session
     */
    protected $_modelSession;
    /**
     * @var PersonalDataFactory
     */
    protected $_model;
    /**
     * @var Config
     */
    protected $_config;
    /**
     * @var DataValid
     */
    protected $_valid;
    /**
     * @var UploaderFactory
     */
    protected $_uploader;



    /**
     * Save constructor.
     * @param Action\Context $context
     * @param Session $session
     * @param PersonalDataFactory $model
     * @param DataValid $helper
     */
    public function __construct(Action\Context $context, Session $session, ImageFactory $model, UploaderFactory $uploader, DataValid $valid)
    {
        $this->_modelSession = $session;
        $this->_model = $model;
        $this->_uploader = $uploader;
//        $this->_config = $config;
        $this->_valid = $valid;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $img = $this->getRequest()->getFiles();
        $valid_result = $this->_valid->validationOfParams($data, ['gallery', 'album']);
        //TODO:chek image size
        $resultRedirect = $this->resultRedirectFactory->create();
        if($valid_result){
            $model = $this->_model->create();
            $model->setAlbum($data['album']);
            $model->setGallery($data['gallery']);
            try{
                $uploader = $this->_uploader->create(['fileId' => 'image']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowRenameFiles(true);
                $uploader->save(self::DIR_ALBUM . DIRECTORY_SEPARATOR . $data['album']. DIRECTORY_SEPARATOR . $data['gallery'], $img['image']['name']);
                $model->setName($uploader->getUploadedFileName());
                $model->save();
                $this->messageManager->addSuccess(__('You saved image.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving a record.'));
            }
        } else {
            $this->messageManager->addError(__('Bad params.'));
        }
        return $resultRedirect->setPath('*/*/');
//        $validResult = $this->_valid->validationOfParams($data, ['name','surname','position','company','telephone']);
//        $resultRedirect = $this->resultRedirectFactory->create();
//        if ($validResult) {
//            $model = $this->_model->create();
//            $id = $this->getRequest()->getParam('entity_id');
//            if ($id) {
//                $model->load($id);
//            } else {
//                unset($data['entity_id']);
//            }
//            $data['registered'] = $this->_date->gmtDate($this->_config->getDateFormat());
//            $model->setData($data);
//            $this->_eventManager->dispatch(
//                'mates_faq_data_prepare_save',
//                ['personaldata' => $model, 'request' => $this->getRequest()]
//            );
//            try {
//                $model->save();
//                $this->messageManager->addSuccess(__('You saved this record.'));
//                $this->_modelSession->setFormData(false);
//                if ($this->getRequest()->getParam('back')) {
//                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId(), '_current' => true]);
//                }
//                return $resultRedirect->setPath('*/*/');
//            } catch (\Magento\Framework\Exception\LocalizedException $e) {
//                $this->messageManager->addError($e->getMessage());
//            } catch (\RuntimeException $e) {
//                $this->messageManager->addError($e->getMessage());
//            } catch (\Exception $e) {
//                $this->messageManager->addException($e, __('Something went wrong while saving a record.'));
//            }
//            $this->_getSession()->setFormData($data);
//            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
//        } else {
//            $this->messageManager->addError(__('Bad params.'));
//        }
//        return $resultRedirect->setPath('*/*/');
    }
}