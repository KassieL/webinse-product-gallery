<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Barcode
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Frontend Action backendName/data/delete
 *
 * @category    Webinse
 * @package     Webinse_Barcode
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
namespace Webinse\Gallery\Controller\Adminhtml\Data;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Webinse\Gallery\Model\ImageFactory;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Delete
 * @package Webinse\Gallery\Controller\Adminhtml\Data
 */
class Delete extends Action
{
    /**
     * base folder
     */
    const DIR_ALBUMS = 'albums';
    /**
     * @var Session
     */
    protected $_modelSession;

    /**
     * @var ImageFactory
     */
    protected $_model;
    /**
     * @var Filesystem
     */
    protected $_filesystem;
    /**
     * @var File
     */
    protected $_file;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param Session $session
     * @param ImageFactory $model
     */
    public function __construct(Action\Context $context, Session $session, ImageFactory $model, Filesystem $filesystem, File $file)
    {
        $this->_modelSession = $session;
        $this->_model = $model;
        $this->_filesystem = $filesystem;
        $this->_file = $file;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $model = $this->_model->create();
        $id = $this->getRequest()->getParam('id');
        if (is_int($id) ? true : (ctype_digit($id))) {
            try {
                $model->load($id);
                if ($model->getId()) {
                    $media_root_dir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
                    if ($this->_file->isExists($media_root_dir . self::DIR_ALBUMS . DIRECTORY_SEPARATOR . $model->getAlbum() . DIRECTORY_SEPARATOR . $model->getGallery() . DIRECTORY_SEPARATOR . $model->getName())) {
                        $this->_file->deleteFile($media_root_dir . self::DIR_ALBUMS . DIRECTORY_SEPARATOR . $model->getAlbum() . DIRECTORY_SEPARATOR . $model->getGallery() . DIRECTORY_SEPARATOR . $model->getName());
                    }
                    $model->delete();
                    $this->messageManager->addSuccess(__('You deleted this image.'));
                } else {
                    $this->messageManager->addException(__('Image does not exist.'));
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving a record.'));
            }
        } else {
            $this->messageManager->addException(__('Incorrect data.'));
        }
        return $resultRedirect->setPath('*/*/');
    }
}