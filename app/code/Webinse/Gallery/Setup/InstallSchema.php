<?php
/**
 * Webinse
 *
 * PHP Version 5.6.23
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
/**
 * Install DB schema script
 *
 * @category    Webinse
 * @package     Webinse_Gallery
 * @author      Webinse Team <info@webinse.com>
 * @copyright   2017 Webinse Ltd. (https://www.webinse.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0
 */
namespace Webinse\Gallery\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table as DdlTable;
use Webinse\Gallery\Api\Data\ImageInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('webinse_gallery_images')
        )->addColumn(
            'image_id',
            DdlTable::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Image ID'
        )->addColumn(
            'name',
            DdlTable::TYPE_TEXT,
            '255',
            [],
            'Name'
        )->addColumn(
            'gallery',
            DdlTable::TYPE_TEXT,
            '255',
            [],
            'Gallery'
        )->addColumn(
            'album',
            DdlTable::TYPE_TEXT,
            '255',
            [],
            'Album'
        )->setComment(
            'Webinse Gallery Table'
        );
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}